class CreateContactInfos < ActiveRecord::Migration[5.2]
  def up
    create_table :contact_infos do |t|
      t.integer :person_id
      t.string :email, :default => '', :null => true
      t.string :cellphone, :default => '', :limit => 10
      
      t.timestamps
    end
    add_index("contact_infos", "person_id")
  end

  def down
    drop_table :contact_infos
  end

end
