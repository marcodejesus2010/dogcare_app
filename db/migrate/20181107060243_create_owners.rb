class CreateOwners < ActiveRecord::Migration[5.2]
  def up
    create_table :owners do |t|
      t.integer :person_id
      t.integer :number_of_visits
      t.integer :number_of_stars

      t.timestamps
    end

    add_index("owners", "person_id")
  end

  def down
    drop_table :owners
  end
end
