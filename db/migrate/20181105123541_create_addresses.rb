class CreateAddresses < ActiveRecord::Migration[5.2]
  def up
    create_table :addresses do |t|
      t.integer :person_id
      t.string :street
      t.string :neighbordhood
      t.string :zip_code
      t.string :city
      t.string :state
      t.string :country

      t.timestamps
    end

    add_index("addresses", "person_id")
  end

  def down
    drop_table :addresses
  end
end
