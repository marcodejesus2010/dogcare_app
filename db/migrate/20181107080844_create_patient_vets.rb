class CreatePatientVets < ActiveRecord::Migration[5.2]
  def up
    create_table :patient_vets do |t|
      t.integer :vet_id
      t.integer :patient_id

      t.timestamps
    end
    add_index("patient_vets",["vet_id", "patient_id"])
  end

  def down
    drop_table :patient_vets
  end
end
