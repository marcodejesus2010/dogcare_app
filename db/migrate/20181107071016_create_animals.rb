class CreateAnimals < ActiveRecord::Migration[5.2]
  def up
    create_table :animals do |t|
      t.string :name
      t.date :dob
      t.string :gender
      t.string :breed
      t.float :size
      t.float :weight
      t.text :notes

      t.timestamps
    end
  end

  def down
    drop_table :animals
  end
end
