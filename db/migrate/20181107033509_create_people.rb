class CreatePeople < ActiveRecord::Migration[5.2]
  def up
    create_table :people do |t|
      t.string :name
      t.date :dob
      t.string :gender

      t.timestamps
    end
  end

  def down
    drop_table :people
  end
end
