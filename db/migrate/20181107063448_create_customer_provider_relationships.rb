class CreateCustomerProviderRelationships < ActiveRecord::Migration[5.2]
  def up
    create_table :customer_provider_relationships do |t|
      t.integer :vet_id
      t.integer :owner_id

      t.timestamps
    end
    add_index("customer_provider_relationships", ["vet_id", "owner_id"])
  end

  def down
    drop_table :customer_provider_relationships
  end
end
