class CreatePatients < ActiveRecord::Migration[5.2]
  def up
    create_table :patients do |t|
      t.integer :animal_id
      t.integer :number_of_visits
      t.text :vaccines
      t.integer :number_of_procedures
      t.text :procedures_description
      t.text :allergies

      t.timestamps
    end
    add_index("patients", "animal_id")
  end

  def down
    drop_table :patients
  end
end
