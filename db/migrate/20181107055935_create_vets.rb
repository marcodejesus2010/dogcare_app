class CreateVets < ActiveRecord::Migration[5.2]
  def up
    create_table :vets do |t|
      t.integer :person_id
      t.integer :years_of_experience
      t.integer :number_of_stars
      t.datetime :opens
      t.datetime :closes

      t.timestamps
    end

    add_index("vets", "person_id")
  end

  def down
    drop_table :vets
  end
end
