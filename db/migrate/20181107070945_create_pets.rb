class CreatePets < ActiveRecord::Migration[5.2]
  def up
    create_table :pets do |t|
      t.integer :animal_id
      t.integer :owner_id
      t.text :habits
      t.text :teraphy
      t.text :behavior

      t.timestamps
    end
    add_index("pets","animal_id")
    add_index("pets", "owner_id ")
  end

  def down
    drop_table :pets
  end
end
