class PatientVet < ApplicationRecord
    belongs_to :vet
    belongs_to :patient
end
