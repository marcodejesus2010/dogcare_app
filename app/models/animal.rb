class Animal < ApplicationRecord
    has_many :pets
    has_many :patients
end
