class Owner < ApplicationRecord
    belongs_to :person
    has_many :customer_provider_relationships
    has_many :vets, :through => :customer_provider_relationships
    has_many :pets
end
