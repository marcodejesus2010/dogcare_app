class Patient < ApplicationRecord
    belongs_to :animal
    has_many :patient_vets
    has_many :vets, :through => :patient_vets
end
