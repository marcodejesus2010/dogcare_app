class Pet < ApplicationRecord
    belongs_to :animal
    belongs_to :owner
end
