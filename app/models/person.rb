class Person < ApplicationRecord
    has_many :addresses
    has_many :contact_infos
    has_many :owners
    has_many :vets
end
