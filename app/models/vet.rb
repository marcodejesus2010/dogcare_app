class Vet < ApplicationRecord
    belongs_to :person
    has_many :customer_provider_relationships
    has_many :owners, :through => :customer_provider_relationships
    has_many :patient_vets
    has_many :patients, :through => :patient_vets
end
